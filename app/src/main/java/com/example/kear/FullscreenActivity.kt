package com.example.kear

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Switch
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.viewpager.widget.ViewPager
import com.example.kear.adapter.IntroAdapter
import com.example.kear.transformers.IntroPageTransformer
import kotlinx.android.synthetic.main.fragment_intro_fragment2.*

class FullscreenActivity : AppCompatActivity(){

    private lateinit var mViewPager: ViewPager
    private lateinit var introAdapter:IntroAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fullscreen)

        introAdapter = IntroAdapter(supportFragmentManager)

        mViewPager = findViewById(R.id.welcome_ViewPager)

        mViewPager.adapter = introAdapter

        mViewPager.setPageTransformer(false, IntroPageTransformer())
    }
}
