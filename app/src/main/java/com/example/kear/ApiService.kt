package com.example.kear

import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.POST
import retrofit2.http.Query

object ApiService {
    private const val API = "http://"
    private var privateApi: PrivateApi? = null

    init {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(API)
            .client(client)
            .build()

        privateApi = retrofit.create(ApiService.PrivateApi::class.java)
    }

//    fun getRoutes(tokenWorker: String, dateForLoad: String): Observable<List<Object>> {
//        return privateApi!!.getRoutes(tokenWorker, dateForLoad)
//    }


    interface PrivateApi {
//        @POST("RoutesInDate")
//        fun getRoutes(@Query("") tokenWorker: String, @Query("") dateForLoad: String): Observable<List<Object>>

    }
}