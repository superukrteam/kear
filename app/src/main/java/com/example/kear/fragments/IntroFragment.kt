package com.example.kear.fragments


import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import com.example.kear.*
import com.google.android.material.button.MaterialButton


const val BACKGROUND_COLOR = "backgroundColor"
const val PAGE = "page"

class IntroFragment : Fragment() {


    private var mBackgroundColor: Int = 0
    private var mPage: Int = 0
    private var layoutResId: Int? = R.layout.fragment_intro1

    private lateinit var sharedPrefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sharedPrefs = context?.getSharedPreferences(SETTINGS_PREF, Context.MODE_PRIVATE)!!

        if (!arguments?.containsKey(BACKGROUND_COLOR)!!)
            throw RuntimeException("Fragment must contain a \"$BACKGROUND_COLOR\" argument!")
        mBackgroundColor = arguments!!.getInt(BACKGROUND_COLOR)

        if (!arguments?.containsKey(PAGE)!!)
            throw RuntimeException("Fragment must contain a \"$PAGE\" argument!")
        mPage = arguments!!.getInt(PAGE)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Select a layout based on the current page
        layoutResId = when (mPage) {
            0 -> R.layout.fragment_intro1
            1 -> R.layout.fragment_intro_fragment2
            else -> R.layout.fragment_intro_fragment2
        }

        // Inflate the layout resource file
        val view = activity!!.layoutInflater.inflate(layoutResId!!, container, false)

        // Set the current page index as the View's tag (useful in the PageTransformer)
        view.tag = mPage

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Set the background color of the root view to the color specified in newInstance()
        val background: View = view.findViewById(R.id.intro_background)
        background.setBackgroundColor(mBackgroundColor)


        if (layoutResId == R.layout.fragment_intro_fragment2) {
            val rightBtn: MaterialButton = view.findViewById(R.id.wf_RightBtn)
            val nightModeSwitch:Switch = view.findViewById(R.id.fr_intro_darkModeSwitch)
            val autoNightMode:Switch = view.findViewById(R.id.fr_intro_darkModeAutoSwitch)


            initThemeListener(nightModeSwitch, autoNightMode)
            initTheme(nightModeSwitch, autoNightMode)

            rightBtn.text = "Поехали!"
            rightBtn.setOnClickListener {
                val intent = Intent(context, MainActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun initThemeListener(nightModeSwitch:Switch,autoNightMode:Switch) {
        nightModeSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                setTheme(AppCompatDelegate.MODE_NIGHT_YES, THEME_DARK)
                autoNightMode.isChecked = false
            } else if (!isChecked) setTheme(AppCompatDelegate.MODE_NIGHT_NO, THEME_LIGHT)

        }
        autoNightMode.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                setTheme(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM, THEME_SYSTEM)
                nightModeSwitch.isChecked = false
            } else if (!isChecked) setTheme(AppCompatDelegate.MODE_NIGHT_NO, THEME_DARK)

        }
    }

    private fun setTheme(themeMode: Int, prefsMode: Int) {
        AppCompatDelegate.setDefaultNightMode(themeMode)
        saveTheme(prefsMode)
    }

    private fun initTheme(nightModeSwitch:Switch,autoNightMode:Switch) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P){
            autoNightMode.visibility = View.VISIBLE
        } else {
            autoNightMode.visibility = View.GONE
        }
        when (getSavedTheme()) {
            THEME_LIGHT -> nightModeSwitch.isChecked = false
            THEME_DARK -> nightModeSwitch.isChecked = true
            THEME_SYSTEM -> autoNightMode.isChecked = true
            THEME_UNDEFINED -> {
                when (resources.configuration.uiMode.and(Configuration.UI_MODE_NIGHT_MASK)) {
                    Configuration.UI_MODE_NIGHT_NO -> nightModeSwitch.isChecked = false
                    Configuration.UI_MODE_NIGHT_YES -> nightModeSwitch.isChecked = true
                    Configuration.UI_MODE_NIGHT_UNDEFINED -> nightModeSwitch.isChecked = true
                }
            }
        }
    }

    private fun saveTheme(theme: Int) = sharedPrefs.edit().putInt(KEY_THEME, theme).apply()

    private fun getSavedTheme() = sharedPrefs.getInt(KEY_THEME, THEME_UNDEFINED)

    companion object {

        @JvmStatic
        fun newInstance(
            backgroundColor: Int,
            page: Int
        ): IntroFragment {
            val frag = IntroFragment()
            val b = Bundle()
            b.putInt(BACKGROUND_COLOR, backgroundColor)
            b.putInt(PAGE, page)
            frag.arguments = b
            return frag
        }
    }


}
