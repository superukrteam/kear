package com.example.kear.fragments

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import com.example.kear.*
import kotlinx.android.synthetic.main.fragment_intro_fragment2.*

class SettingsFragment : Fragment() {

    private lateinit var sharedPrefs: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        sharedPrefs = context?.getSharedPreferences(SETTINGS_PREF, Context.MODE_PRIVATE)!!

        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        initThemeListener(fr_intro_darkModeSwitch,fr_intro_darkModeAutoSwitch)
        initTheme(fr_intro_darkModeSwitch,fr_intro_darkModeAutoSwitch)

    }

    private fun initThemeListener(nightModeSwitch: Switch, autoNightMode: Switch) {
        nightModeSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                setTheme(AppCompatDelegate.MODE_NIGHT_YES, THEME_DARK)
                autoNightMode.isChecked = false
            } else if (!isChecked) setTheme(AppCompatDelegate.MODE_NIGHT_NO, THEME_LIGHT)

        }
        autoNightMode.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                setTheme(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM, THEME_SYSTEM)
                nightModeSwitch.isChecked = false
            } else if (!isChecked) setTheme(AppCompatDelegate.MODE_NIGHT_NO, THEME_DARK)

        }
    }

    private fun setTheme(themeMode: Int, prefsMode: Int) {
        AppCompatDelegate.setDefaultNightMode(themeMode)
        saveTheme(prefsMode)
    }

    private fun initTheme(nightModeSwitch: Switch, autoNightMode: Switch) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            autoNightMode.visibility = View.VISIBLE
        } else {
            autoNightMode.visibility = View.GONE
        }
        when (getSavedTheme()) {
            THEME_LIGHT -> nightModeSwitch.isChecked = false
            THEME_DARK -> nightModeSwitch.isChecked = true
            THEME_SYSTEM -> autoNightMode.isChecked = true
            THEME_UNDEFINED -> {
                when (resources.configuration.uiMode.and(Configuration.UI_MODE_NIGHT_MASK)) {
                    Configuration.UI_MODE_NIGHT_NO -> nightModeSwitch.isChecked = false
                    Configuration.UI_MODE_NIGHT_YES -> nightModeSwitch.isChecked = true
                    Configuration.UI_MODE_NIGHT_UNDEFINED -> nightModeSwitch.isChecked = true
                }
            }
        }
    }

    private fun saveTheme(theme: Int) = sharedPrefs.edit().putInt(KEY_THEME, theme).apply()

    private fun getSavedTheme() = sharedPrefs.getInt(KEY_THEME, THEME_LIGHT)

    companion object {
        @JvmStatic
        fun newInstance() =
            SettingsFragment()
    }
}
