package com.example.kear.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.DefaultItemAnimator
import com.example.kear.R
import kotlinx.android.synthetic.main.main_fragment.*

const val ARG_CURRENT_USER = "ARG_CURRENT_USER"

class MainFragment : Fragment(), Toolbar.OnMenuItemClickListener {
    override fun onMenuItemClick(item: MenuItem?): Boolean {
        val id = item?.itemId
        when (id) {
            R.id.item_clearDatabase -> {

            }
            R.id.item_toSettingsBtn -> {
                fragmentManager?.beginTransaction()
                    ?.replace(R.id.container, SettingsFragment.newInstance())
                    ?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    ?.addToBackStack("tag")
                    ?.commitNow()
            }
            R.id.developer_page -> {
                fragmentManager?.beginTransaction()
                    ?.replace(R.id.container, DevelperFragment.newInstance())
                    ?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    ?.addToBackStack("tag")
                    ?.commitNow()
            }
        }
        return false
    }

    companion object {
        fun newInstance() = MainFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerMessages.itemAnimator = DefaultItemAnimator()

        toolbar.inflateMenu(R.menu.main_toolbar_menu)
        toolbar.inflateMenu(R.menu.developer_menu)
        toolbar.setOnMenuItemClickListener(this)


    }


    override fun onStop() {
        super.onStop()
    }
}
