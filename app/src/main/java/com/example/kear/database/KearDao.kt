package com.example.kear.database

import androidx.room.*
import com.example.kear.models.KearModel
import io.reactivex.Flowable

@Dao
interface KearDao {
    @Query("SELECT * FROM KearModel")
    fun getKear(): Flowable<KearModel>

    @Insert
    fun update(vararg kearModel: KearModel)
}