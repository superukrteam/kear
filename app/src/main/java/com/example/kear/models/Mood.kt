package com.example.kear.models

data class Mood(
    var moodId: Int,
    var title: String,
    var image: String,
    var mood_level:MoodLevel
)