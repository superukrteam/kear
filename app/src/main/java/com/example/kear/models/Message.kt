package com.example.kear.models

data class Message(
    var messageId: Int,
    var keyWordId: Int,
    var answer: String
)