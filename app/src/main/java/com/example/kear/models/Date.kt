package com.example.kear.models

data class Date(
    var dateId: Int,
    var date: String
)