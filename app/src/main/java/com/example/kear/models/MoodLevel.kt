package com.example.kear.models

data class MoodLevel(
    var moodLevelId: Int,
    var moodId: Int,
    var coefficien: String,
    var image:String
)