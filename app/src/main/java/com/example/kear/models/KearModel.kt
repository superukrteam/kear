package com.example.kear.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class KearModel (
    @PrimaryKey
    var name:String,
    var happiness:Int,      //0 печаль\уныние, 100 счастливая
    var boredom:Int,        //0 игривость, 100 скучно
    var sexuality:Int,      //0 отвращение, 100 кокетство
    var talkativeness:Int   //0 не хочет говорить, 100 болтливость
)