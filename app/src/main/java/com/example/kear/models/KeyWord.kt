package com.example.kear.models

data class KeyWord(
    var keyWordId: Int,
    var keyWord: String,
    var changeMood: Boolean? = false,
    var image: String? = null
)