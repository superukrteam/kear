package com.example.kear.models

data class DayOfWeek(
    var dayOfWeekId: Int,
    var dayOfWeek: String
)