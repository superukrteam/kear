package com.example.kear.models

data class MoodWord(
    var moodWordId: Int,
    var keyWordId: Int,
    var coefficient: Int,
    var sign:String
)